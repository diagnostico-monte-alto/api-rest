<?php
/**
 * Description of EntityAPI
 *
 * @author WebDev
 */
abstract class EntityAPI {
    const SYNCRO_OK = 200;
    const ERROR_DUPLICATED = -98;
    const ERROR_UNKNOWN = -99;
    const ERROR_FOREING_KEY = 202;
    const ENTITY_DELETED = 204;
    const ERROR_NOT_MODIFIED = 304;
    
    protected $db = null;
    protected $fields = null;
    
    public function API(){
        header('Content-Type: application/JSON');    
        $method = $_SERVER['REQUEST_METHOD'];
        
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function checkFields($objs) {
        if (empty($objs)){return false;}
        foreach($this->fields as $fld) {
            $exist = false;
            while (($obj = current($objs) !== false) && !$exist) {
                if (key($objs) == $fld) { 
                    $exist = true; 
                    break;
                }
                next($objs);
            }
            reset($objs);
            if(!$exist) {
                return false; 
            }
        }
        return true;
    }
    
    function processDelete() {
        $id = filter_input(INPUT_GET, 'id');
        if (!$id) {
            $this->response(400);
            exit;
        }
        if($this->db->delete($id) === 0) {
            $this->response(200, "success", "The entity was deleted");
        } else {
            $this->response(202, "error", "no permitido");
        }
    }
    
//    function processDelete() {
//        $id = filter_input(INPUT_GET, 'id');
//        if (!$id) {
//            $this->response(400);
//            exit;
//        }
//        $this->db->delete($id);
//        $this->response(200, "success", "The entity was deleted");
//    }
    
    function response($code=200, $status="", $message="") {
        http_response_code($code);
        if( !empty($status) && !empty($message) ){
            $response = array("status" => $status ,"message"=>$message);  
            echo json_encode($response,JSON_PRETTY_PRINT);    
        }  
    }
}
