<?php
/**
 * Description of UsuarioDb
 *
 * @author WebDev
 */
class UsuarioDB extends EntityDB {
   const TABLE = 'usuarios';
   protected $mysqli;
    
    public function getUsuario($id=0){
        $query = "SELECT u.id, u.usuario, u.contrasena, u.idrol, r.rol, u.nombre, 
                u.apellido, u.fotourl, u.fecultmodif
            FROM usuarios u 
            LEFT JOIN roles r ON r.id = u.idrol
            WHERE u.id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $usuario;
    }
    
    public function getUsuarios(){
        $query = "SELECT u.id, u.usuario, u.contrasena, u.idrol, r.rol, u.nombre, 
                u.apellido, u.fotourl, u.fecultmodif
            FROM usuarios u 
            LEFT JOIN roles r ON r.id = u.idrol;";
        $result = $this->mysqli->query($query);
        $usuarios = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $usuarios;
    }
    
    public function login($usuario='', $contrasena=''){
        $query = "SELECT * FROM usuarios WHERE usuario='$usuario' AND contrasena=PASSWORD('$contrasena');";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $r = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return (count($r)>0) ? $r : -1;
    }
    
    public function resetPass($id, $contrasena) {        
        $query = "UPDATE usuarios SET contrasena = PASSWORD('$contrasena') WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function resetPassword($id, $newcontrasena) {        
        $query = "UPDATE usuarios SET contrasena = PASSWORD('$newcontrasena') WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function insert($usuario='', $contrasena='', $apellido='', $nombre='',  
            $idrol=0, $fotourl=''){
        $query = "INSERT INTO usuarios 
                (usuario, contrasena, apellido, nombre, 
                idrol, fotourl, fecultmodif)
            VALUES
                ('$usuario', PASSWORD('$contrasena'), '$apellido', '$nombre', 
                $idrol, '$fotourl', NOW());";
//        var_dump($query);
//        exit;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    // No se actualiza la contraseña ya que se hace por reset o por cambio de contraseña
    public function update($id, 
            $usuario, $apellido, $nombre, 
            $idrol, $fotourl) {
        $query = "UPDATE usuarios SET usuario='$usuario', apellido='$apellido', nombre='$nombre', 
                idrol=$idrol, fotourl='$fotourl', fecultmodif=NOW()
            WHERE id = $id;"; 
//        var_dump($query);
//        exit;
        if($this->checkIntID( self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM usuarios WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return 0;
    }
    
//    public function authenticate($usuario='', $contrasena=''){
//        $query = "SELECT id FROM usuarios WHERE usuario='$usuario' AND contrasena='$contrasena'";
//        var_dump($query);
//        $stmt = $this->mysqli->prepare($query);
//        $stmt->execute();
//        $result = $stmt->get_result();
//        $usuario = $result->fetch_all(MYSQLI_ASSOC);
//        $stmt->close();
//        
//        return $usuario;
//    }
}