<?php
/**
 * Description of ModuloXRolDB
 *
 * @author meza
 */
class ModuloXRolDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'modulosxroles';
    
    public function getById($id=0){
//        $query = "SELECT e.id, e.idpaciente, CONCAT(p.apellido, ', ', p.nombre) AS paciente,
//                e.idprofesional, CONCAT(r.apellido, ', ', r.nombres) AS profesional, 
//                e.descripcion, e.fecestudio 
//            FROM estudios e 
//            LEFT JOIN pacientes p ON e.idpaciente = p.id
//            LEFT JOIN profesionales r ON e.idprofesional = r.id
//            WHERE e.id = '$id';";
////        var_dump($query);
//        $stmt = $this->mysqli->prepare($query);
//        $stmt->execute();
//        $result = $stmt->get_result();
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $stmt->close();
//        return $entity;
    }
    
    public function getModulosByIdRol($idrol) {
        $query = "SELECT x.id, x.idrol, r.rol, x.idmodulo, m.modulo, m.componente,
                x.ver, x.editar, x.eliminar
            FROM modulosxroles x 
            LEFT JOIN roles r ON r.id = x.idrol
            LEFT JOIN modulos m ON m.id = x.idmodulo
            WHERE x.idrol = $idrol
            ORDER BY m.modulo;";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList(){
//        $query = "SELECT e.id, e.idpaciente, CONCAT(p.apellido, ', ', p.nombre) AS paciente,
//                e.idprofesional, CONCAT(r.apellido, ', ', r.nombres) AS profesional, 
//                e.descripcion, e.fecestudio 
//            FROM estudios e 
//            LEFT JOIN pacientes p ON e.idpaciente = p.id
//            LEFT JOIN profesionales r ON e.idprofesional = r.id;";
////            var_dump($query);
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
    }
    
//    public function insert( $id='', $idsesion='', $idpaciente='',  $idprofesional='',  $idtipoevento=-1,  $evento='', $horaevento=''){
//        $query="INSERT INTO " . self::TABLE . " (
//                id, idsesion, idpaciente, idprofesional, idtipoevento, evento, horaevento, fecultmodif) 
//                VALUES (
//                '$id', '$idsesion', '$idpaciente', '$idprofesional', $idtipoevento, '$evento', '$horaevento', NOW());";
////        var_dump($query);
//        $stmt = $this->mysqli->prepare($query);
//        $r = $stmt->execute();        
//        $stmt->close();
//        $lastid = $this->mysqli->insert_id;
//        return $lastid;
//    }
    
    public function update($id='', 
            $ver=1, $editar=1, $eliminar=1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE modulosxroles SET 
                    ver = $ver, editar = $editar, eliminar = $eliminar, 
                    fecultmodif = NOW() 
                WHERE id = $id;";
//            var_dump($query);
//            exit;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}