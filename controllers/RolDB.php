<?php
/**
 * Description of RolDB
 *
 * @author meza
 */
class RolDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'roles';
    
    public function getById($id=0){
        $query = "SELECT *
            FROM roles
            WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT * 
            FROM roles ;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($rol=''){
        $query = "CALL new_rol('$rol', @p_oresult);";
        $r = $this->mysqli->query($query);
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
//        var_dump( $r);
        return $row['p_oresult'];
//        $stmt = $this->mysqli->prepare($query);
//        $r = $stmt->execute();        
//        $stmt->close();
//        $lastid = $this->mysqli->insert_id;
//        return $lastid;
    }
    
    public function update($id='', $rol='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE roles SET 
                     rol = '$rol'
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM roles WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return 0;
    }
}