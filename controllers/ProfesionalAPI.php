<?php
/**
 * Description of ProfesionalAPI
 *
 * @author meza
 */
class ProfesionalAPI extends EntityAPI {
    const GET_LISTAESTUDIOS = 'listaestudios';
    const POST_FOTO = 'foto';
    const POST_DNI = 'dni';
    const POST_TITULO = 'titulo';
    const POST_MATRICULA = 'matricula';
    const PUT_LOGIN = 'login';
    const PUT_LOGINSOCIAL = 'loginsocial';
    const PUT_BLOQUEO = 'bloqueo';
    const PUT_APROBACION = 'aprobacion';
    const API_ACTION = 'profesional';

    public function __construct() {
        $this->db = new ProfesionalDB();
        $this->fields = [];
        array_push($this->fields, 
                'idsexo',
                'idpais',
                'idprovincia',
                'usuario',
                'email',
                'contrasena',
                'apellido',
                'nombre',
                'dni',
                'fecnacimiento',
                'notificacion',
                'titulo',
                'universidad',
                'microbiografia',
                'credenciales',
                'precio',
                'fotourl',
                'dniurl',
                'titulourl',
                'matriculaurl',
                'fecaprobado',
                'fecmodificado',
                'fecregistro',
                'bloqueoadministrativo',
                'idsocial',
                'especialidades');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isListaEstudios = isset($id) ? $id === self::GET_LISTAESTUDIOS: false;
        
        if ($isListaEstudios) {
            $id = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByListaEstudios($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        $isUploadFoto = isset($id) ? $id === self::POST_FOTO : false;
        $isUploadDni = isset($id) ? $id === self::POST_DNI : false;
        $isUploadTitulo = isset($id) ? $id === self::POST_TITULO : false;
        $isUploadMatricula = isset($id) ? $id === self::POST_MATRICULA : false;
        
        if ($isUploadFoto) {
            $id = filter_input(INPUT_GET, 'fld1');
            $filePath = $r = $this->uploadFile($id, 'prof/', 'ava_');
            if($filePath === 500) {
                $this->response(500,"false", "Directorio destino no habilitado para escritura");
                exit;
            } else if($filePath === 501) {
                $this->response(500, "false", "Archivo no subido!!");
                exit;
            }
            $r = $this->db->uploadFileFoto($id, $filePath);
            if($r){ $this->response(200,"succes", $r); } 
            else { $this->response(204); }
            exit;
        }
        
        if ($isUploadDni) {
            $id = filter_input(INPUT_GET, 'fld1');
            $filePath = $r = $this->uploadFile($id, 'prof/', 'dni_');
            if($filePath === 500) {
                $this->response(500,"false", "Directorio destino no habilitado para escritura");
                exit;
            } else if($filePath === 501) {
                $this->response(500, "false", "Archivo no subido!!");
                exit;
            }
            $r = $this->db->uploadFileDni($id, $filePath);
            if($r){ $this->response(200,"succes",$r); } 
            else { $this->response(204); }
            exit;
        }
        
        if ($isUploadTitulo) {
            $id = filter_input(INPUT_GET, 'fld1');
            $filePath = $r = $this->uploadFile($id, 'prof/', 'tit_');
            if($filePath === 500) {
                $this->response(500,"false", "Directorio destino no habilitado para escritura");
                exit;
            } else if($filePath === 501) {
                $this->response(500, "false", "Archivo no subido!!");
                exit;
            }
            $r = $this->db->uploadFileTitulo($id, $filePath);
            if($r){ $this->response(200,"succes",$r); } 
            else { $this->response(204); }
            exit;
        }
        
        if ($isUploadMatricula) {
            $id = filter_input(INPUT_GET, 'fld1');
            $filePath = $r = $this->uploadFile($id, 'prof/', 'mat_');
            if($filePath === 500) {
                $this->response(500,"false", "Directorio destino no habilitado para escritura");
                exit;
            } else if($filePath === 501) {
                $this->response(500, "false", "Archivo no subido!!");
                exit;
            }
            $r = $this->db->uploadFileMatricula($id, $filePath);
            if($r){ $this->response(200,"succes",$r); } 
            else { $this->response(204); }
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = (isset($obj->id)) ? $obj->id : '';
        $r = $this->db->insert($id,
                $obj->idsexo, $obj->idpais, $obj->idprovincia, 
                $obj->usuario, $obj->email, $obj->contrasena, 
                $obj->apellido, $obj->nombre,  $obj->dni, 
                $obj->fecnacimiento, $obj->notificacion, $obj->titulo, 
                $obj->universidad, $obj->microbiografia, $obj->credenciales, 
                $obj->precio, $obj->fotourl, $obj->dniurl, 
                $obj->titulourl, $obj->matriculaurl, $obj->fecaprobado, 
                $obj->fecmodificado, $obj->fecregistro, $obj->bloqueoadministrativo, 
                $obj->sesionesgratis, $obj->idsocial);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(205,$this->db->getLastError(),"No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        
//        if(!$this->checkFields($obj)) {
//            $this->response(422,"error","The property is not defined");
//            exit;
//        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $isLogin = isset($id) ? $id === self::PUT_LOGIN : false;
        $isLoginSocial = isset($id) ? $id === self::PUT_LOGINSOCIAL : false;
        $isBloqueo = isset($id) ? $id === self::PUT_BLOQUEO : false;
        $isAprobacion = isset($id) ? $id === self::PUT_APROBACION : false;
        
        if($isLogin) { 
            $r = $this->db->login($obj->usuario, $obj->contrasena);
            if($r !== -1){ $this->response(200,"success",$r); } 
            else { $this->response(204); }
        } elseif($isLoginSocial) {
//            var_dump($obj->idsocial);
            $r = $this->db->loginSocial($obj->id, $obj->idsocial, $obj->fotourl);
            if($r !== -1){ $this->response(200,"success",$r); } 
            else { $this->response(204); }
        } elseif($isBloqueo) {
            $r = $this->db->updateBloqueo(
                    $obj->id, $obj->bloqueoadministrativo);
            if($r !== -1){ $this->response(200,"success",$r); } 
            else { $this->response(204); }
        } elseif($isAprobacion) {
            $r = $this->db->updateAprobacion($obj->id);
            if($r !== -1){ $this->response(200,"success",$r); } 
            else { $this->response(204); }
        } else {
            $r = $this->db->update($id,
                    $obj->idsexo, $obj->idpais, $obj->idprovincia, 
                    $obj->usuario, $obj->email,$obj->contrasena, 
                    $obj->apellido, $obj->nombre, $obj->dni, 
                    $obj->fecnacimiento, $obj->notificacion, $obj->titulo, 
                    $obj->universidad, $obj->microbiografia, $obj->credenciales, 
                    $obj->precio, $obj->fotourl, $obj->dniurl, 
                    $obj->titulourl, $obj->matriculaurl, $obj->fecaprobado, 
                    $obj->fecmodificado, $obj->fecregistro, $obj->bloqueoadministrativo, 
                    $obj->sesionesgratis, $obj->especialidades);
            if($r) { $this->response(200,"success","Record updated"); }
            else { $this->response(204,"success","Record not updated");}
        }
    }
    
    function uploadFile($id = '', $dir='', $prefix='') {
        if (isset($_FILES['file'])) {
            $rootPath = $this->getRootPath();
            $originalName = $_FILES['file']['name'];
            $ext = '.' . pathinfo($originalName, PATHINFO_EXTENSION);
            $generatedName = $dir . $prefix . $id . $ext;
            $filePath = $rootPath . $generatedName;

            if (!is_writable($rootPath)) {
                return 500;
            }

            if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
                return $generatedName;
            }
        }
        else {
            return 501;
        }
    }
    
    function getRootPath() {
        $ini = parse_ini_file('conf.ini', true);
        if($ini['enviroment']['prod']==1) {
            $conf=$ini['prod'];
        } else {
            $conf=$ini['debug'];
        }
        return $conf['imagestock'];
    }
    
    function getRootUrl() {
        $ini = parse_ini_file('conf.ini', true);
        if($ini['enviroment']['prod']==1) {
            $conf=$ini['prod'];
        } else {
            $conf=$ini['debug'];
        }
        return $conf['imagestockurl'];
    }
}