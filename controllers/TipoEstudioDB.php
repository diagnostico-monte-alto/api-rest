<?php
/**
 * Description of TipoEstudioDB
 *
 * @author meza
 */
class TipoEstudioDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tiposestudios';
    
    public function getById($id=0){
        $query = "SELECT *
            FROM tiposestudios
            WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT * 
            FROM tiposestudios ;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($tipoestudio=''){
        $query="INSERT INTO tiposestudios (tipoestudio) VALUES ('$tipoestudio');";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id='', $tipoestudio='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE tiposestudios SET 
                    tipoestudio = '$tipoestudio'
                WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM tiposestudios WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return 0;
    }
}