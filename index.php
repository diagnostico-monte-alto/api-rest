<?php
    define('TIMEZONE', 'America/Argentina/Buenos_Aires');
    date_default_timezone_set(TIMEZONE);

    require_once "./controllers/EntityAPI.php";
    require_once "./controllers/EntityDB.php";
    require_once "./controllers/EstudioAPI.php";
    require_once "./controllers/EstudioDB.php";
    require_once "./controllers/ModuloXRolAPI.php";
    require_once "./controllers/ModuloXRolDB.php";
    require_once "./controllers/ProfesionalAPI.php";
    require_once "./controllers/ProfesionalDB.php";
    require_once "./controllers/RolAPI.php";
    require_once "./controllers/RolDB.php";
    require_once "./controllers/TipoEstudioAPI.php";
    require_once "./controllers/TipoEstudioDB.php";
    require_once "./controllers/UsuarioAPI.php";
    require_once "./controllers/UsuarioDB.php";
    /*
    $request_headers = apache_request_headers();
    $http_origin = $request_headers['Origin'];
    $allowed_http_origins = array(
        "http://localhost:4200",
        "http://localhost" ,
        "http://181.164.21.99");
    if (in_array($http_origin, $allowed_http_origins)){  
        header("Access-Control-Allow-Origin: " . $http_origin);
    }*/
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Pragma');
    
    $action = $_GET['action'];
    $api = NULL;
    
    switch($action) {
        case EstudioAPI::API_ACTION:
            $api = new EstudioAPI();
            break;
        case ModuloXRolAPI::API_ACTION:
            $api = new ModuloXRolAPI();
            break;
        case ProfesionalAPI::API_ACTION:
            $api = new ProfesionalAPI();
            break;
        case RolAPI::API_ACTION:
            $api = new RolAPI();
            break;
        case TipoEstudioAPI::API_ACTION:
            $api = new TipoEstudioAPI();
            break;
        case UsuarioAPI::API_ACTION:
            $api = new UsuarioAPI();
            break;
        default:
            echo 'METODO NO SOPORTADO';
            break;
    }
    
    if ($api != NULL) {
        $api->API();
    }
